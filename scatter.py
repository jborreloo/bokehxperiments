from bokeh.io import output_notebook, show
from bokeh.sampledata.iris import flowers
flowers.head()
from bokeh.charts import Scatter
from bokeh.plotting import output_file

output_file("flowers.html", title="flowers")
p = Scatter(flowers, x='petal_length', y='petal_width', color='species', marker='species', legend='top_left')
show(p)
