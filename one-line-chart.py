from bokeh.io import output_notebook, show
output_notebook()
from bokeh.charts import BoxPlot
from bokeh.sampledata.autompg import autompg as data
from bokeh.plotting import output_file

output_file("boxplot.hmtl", title="box plot example")
box = BoxPlot(data, values='mpg', label='cyl', title="MPG Summary (grouped by CYL)")
show(box)
